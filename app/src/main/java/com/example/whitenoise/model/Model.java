package com.example.whitenoise.model;

import com.example.whitenoise.state.State;
import com.example.whitenoise.utils.Utils;
import com.google.common.collect.Iterables;
import com.google.common.primitives.Longs;

import java.io.*;
import java.util.*;
import java.util.stream.Collectors;

public class Model extends State {
    public Model() {
        super();
    }

    public Model(String path) throws IOException {
        super(path);
    }

    public byte[] encodeDecode(byte[] message, String key) throws IOException, IllegalArgumentException, NoSuchElementException {
        byte[] gamma = generateGamma(message.length, key);
        assert(gamma.length >= message.length);

        byte[] result = new byte[message.length];
        for (int i = 0; i < message.length; i++) {
            result[i] = (byte) (message[i] ^ gamma[i]);
        }

        return result;
    }

    private String getPath(String emoji) throws NoSuchElementException {
        return getEmoji(emoji).getPath();
    }

    private long getHash(String emoji) throws NoSuchElementException {
        return getEmoji(emoji).getLongHash();
    }

    public byte[] generateGamma(long length, String key) throws IOException, IllegalArgumentException, NoSuchElementException {
        if (!Utils.containsOnlyEmojis(key)) {
            throw new IllegalArgumentException("key should only contain emojis");
        }
        if (key.isEmpty()) {
            throw new IllegalArgumentException("key should not be empty");
        }

        List<String> emojis = Utils.splitStringIntoEmojis(key);
        String[] paths = Iterables.toArray(emojis.stream().map(this::getPath).collect(Collectors.toList()), String.class);
        long[] positions = Longs.toArray(emojis.stream().map(this::getHash).collect(Collectors.toList()));

        return generateGamma(length, paths, positions);
    }

    private byte[] readBlock(String path, long index) throws IOException {
        long blocksCount = Utils.numberOfBlocks(path, BLOCK_SIZE);
        long blockIndex = Math.floorMod(index, blocksCount);
        return Utils.readBlock(path, blockIndex, BLOCK_SIZE);
    }

    public byte[] generateGamma(long length, String[] paths, long[] positions) throws IOException {
        ByteArrayOutputStream outStream = new ByteArrayOutputStream();
        long blocksCount = Utils.numberOfBlocks(length, BLOCK_SIZE);

        assert(paths.length != 0 && paths.length == positions.length);

        // Yes, here it comes, the gimmick, the twist, the old switcheroo.
        for (long i = 0; i < blocksCount; i++) {
            byte[] gamma = readBlock(paths[0], positions[0]);

            for (int j = 1; j < paths.length; j++) {
                byte[] block = readBlock(paths[j], positions[j]);
                gamma = Utils.mixedHash(block, gamma);
            }

            byte[] positionGamma = gamma.clone();
            for (int j = 0; j < paths.length; j++) {
                byte[] block = readBlock(paths[j], positions[j]);
                positionGamma = Utils.mixedHash(block, positionGamma);
                positionGamma = Utils.mixedHash(positions[j], positionGamma);
            }

            for (int j = 0; j < paths.length; j++) {
                byte[] block = readBlock(paths[j], positions[j]);
                block = Utils.mixedHash(positionGamma, block);
                block = Utils.mixedHash(positions[j], block);
                positions[j] = Utils.bytesLongHash(block);
            }

            outStream.write(gamma);
        }

        return outStream.toByteArray();
    }
}
