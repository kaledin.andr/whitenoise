package com.example.whitenoise.application;

import com.example.whitenoise.model.Model;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.RadioButton;
import android.widget.TextView;
import android.widget.Toast;

import com.example.whitenoise.R;
import com.google.common.io.BaseEncoding;

import java.io.File;
import java.io.IOException;
import java.nio.charset.StandardCharsets;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.NoSuchElementException;

public class MainActivity extends AppCompatActivity {
    Model model;
    EditText emojiEditText;
    EditText inputMessageEditText;
    Button encryptMessageButton;
    Button addMappingButton;
    TextView outputMessageTextView;

    RadioButton inputHexRadioButton;
    RadioButton inputUnicodeRadioButton;
    RadioButton outputHexRadioButton;
    RadioButton outputUnicodeRadioButton;

    protected String getConfigPath() throws IOException {
        File file = new File(getExternalFilesDir(null), "whitenoise_config.json");
        return file.getCanonicalPath();
    }

    private void loadModel() {
        try {
            model = new Model(getConfigPath());
        } catch (Exception ignore) {
            finish();
        }
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        addMappingButton = findViewById(R.id.startMappingIntent);
        addMappingButton.setOnClickListener(this::startMappingActivity);

        emojiEditText = findViewById(R.id.inputEmoji);
        inputMessageEditText = findViewById(R.id.inputText);

        inputHexRadioButton = findViewById(R.id.inputHex);
        inputUnicodeRadioButton = findViewById(R.id.inputUnicode);
        outputHexRadioButton = findViewById(R.id.outputHex);
        outputUnicodeRadioButton = findViewById(R.id.outputUnicode);

        encryptMessageButton = findViewById(R.id.encryptMessageButton);
        encryptMessageButton.setOnClickListener(this::encryptMessage);

        outputMessageTextView = findViewById(R.id.outputText);
    }

    private void encryptMessageHelper(View view) throws IOException, IllegalArgumentException, NoSuchElementException {
        loadModel();

        String emojis = emojiEditText.getText().toString();
        String textForEncryption = inputMessageEditText.getText().toString();

        byte[] inputBytes;
        if (inputHexRadioButton.isChecked()) {
            inputBytes = BaseEncoding.base16().lowerCase().decode(textForEncryption.toLowerCase());
        } else if (inputUnicodeRadioButton.isChecked()) {
            inputBytes = textForEncryption.getBytes(StandardCharsets.UTF_8);
        } else {
            finish();
            return;
        }

        byte[] outputBytes = model.encodeDecode(inputBytes, emojis);

        String output;
        if (outputHexRadioButton.isChecked()) {
            output = BaseEncoding.base16().encode(outputBytes);
        } else if (outputUnicodeRadioButton.isChecked()) {
            output = new String(outputBytes);
        } else {
            finish();
            return;
        }

        outputMessageTextView.setText(output);
    }

    private void encryptMessage(View view) {
        try {
            encryptMessageHelper(view);
        } catch (Exception exception) {
            Toast.makeText(this, exception.getMessage(), Toast.LENGTH_SHORT).show();
        }
    }

    private void startMappingActivity(View view) {
        Intent mappingActivityIntent = new Intent(MainActivity.this, MappingActivity.class);
        startActivity(mappingActivityIntent);
    }

}