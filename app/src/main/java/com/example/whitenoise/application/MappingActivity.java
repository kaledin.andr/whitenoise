package com.example.whitenoise.application;

import com.example.whitenoise.model.Model;

import android.content.SharedPreferences;
import android.os.Bundle;
import android.view.MenuItem;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ListView;
import android.widget.Toast;

import androidx.appcompat.app.ActionBar;
import androidx.appcompat.app.AppCompatActivity;

import com.example.whitenoise.R;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

public class MappingActivity extends AppCompatActivity {
    Model model;

    Map<String, ?> mappings;
    List<String> stringMappings;
    SharedPreferences sharedPreferences;

    ListView listView;
    ArrayAdapter<String> arrayAdapter;

    EditText inputAddKeyEditText;
    EditText inputAddFilenameEditText;
    EditText inputDeleteKeyEditText;

    Button addMappingButton;
    Button deleteMappingButton;

    protected String getConfigPath() throws IOException {
        File file = new File(getExternalFilesDir(null), "whitenoise_config.json");
        return file.getCanonicalPath();
    }

    private void loadModel() {
        try {
            model = new Model(getConfigPath());
        } catch (Exception ignore) {
            finish();
        }
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_mapping);

        ActionBar actionBar = getSupportActionBar();
        if (actionBar != null) {
            actionBar.setHomeButtonEnabled(true);
            actionBar.setDisplayHomeAsUpEnabled(true);
            actionBar.setTitle("Key to File Mappings");
        }

        sharedPreferences = getSharedPreferences("mappings", MODE_PRIVATE);

        mappings = sharedPreferences.getAll();
        stringMappings = mappingsToStringList(mappings);

        listView = findViewById(R.id.mappingListView);
        arrayAdapter = new ArrayAdapter<>(this, android.R.layout.simple_list_item_1, stringMappings);
        listView.setAdapter(arrayAdapter);

        inputAddKeyEditText = findViewById(R.id.inputAddKey);
        inputAddFilenameEditText = findViewById(R.id.inputAddFilename);
        inputDeleteKeyEditText = findViewById(R.id.inputDeleteKey);

        addMappingButton = findViewById(R.id.addMapping);
        addMappingButton.setOnClickListener(this::addMapping);
        deleteMappingButton = findViewById(R.id.deleteMapping);
        deleteMappingButton.setOnClickListener(this::deleteMapping);
    }

    private void addMapping(View view) {
        loadModel();

        String key = inputAddKeyEditText.getText().toString();
        String value = inputAddFilenameEditText.getText().toString();

        if (key.isEmpty()) {
            Toast.makeText(this, "Key is empty", Toast.LENGTH_SHORT).show();
            return;
        }
        if (value.isEmpty()) {
            Toast.makeText(this, "Filename is empty", Toast.LENGTH_SHORT).show();
            return;
        }

        try {
            model.putEmoji(key, value);
        } catch (Exception exception) {
            Toast.makeText(this, exception.getMessage(), Toast.LENGTH_SHORT).show();
            return;
        }

        addMappingToPreferences(key, value);
        refreshList();

        inputAddKeyEditText.setText("");
        inputAddFilenameEditText.setText("");
    }

    private void deleteMapping(View view) {
        loadModel();

        String key = inputDeleteKeyEditText.getText().toString();
        if (key.isEmpty()) {
            Toast.makeText(this, "Key is empty", Toast.LENGTH_SHORT).show();
            return;
        }

        try {
            model.removeEmoji(key);
        } catch (Exception exception) {
            Toast.makeText(this, exception.getMessage(), Toast.LENGTH_SHORT).show();
            return;
        }

        deleteMappingFromPreferences(key);
        refreshList();

        inputDeleteKeyEditText.setText("");
    }

    private void addMappingToPreferences(String key, String value) {
        if (sharedPreferences.contains(key)) {
            Toast.makeText(this, "Key is already in use", Toast.LENGTH_SHORT).show();
            return;
        }
        SharedPreferences.Editor editor = sharedPreferences.edit();
        editor.putString(key, value);
        editor.commit();
    }

    private void deleteMappingFromPreferences(String key) {
        if (!sharedPreferences.contains(key)) {
            Toast.makeText(this, "Key is not present", Toast.LENGTH_SHORT).show();
            return;
        }
        SharedPreferences.Editor editor = sharedPreferences.edit();
        editor.remove(key);
        editor.commit();
    }

    private void refreshList() {
        mappings = sharedPreferences.getAll();
        stringMappings = mappingsToStringList(mappings);

        this.recreate();
    }

    private List<String> mappingsToStringList(Map<String, ?> mappings) {
        List<String> mappingsStringList = new ArrayList<>();
        for (String key : mappings.keySet()) {
            if (key.isEmpty()) {
                continue;
            }
            mappingsStringList.add(key + " -> " + mappings.get(key));
        }
        return mappingsStringList;
    }

    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                this.finish();
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }
}
