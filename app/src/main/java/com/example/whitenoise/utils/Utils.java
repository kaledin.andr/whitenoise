package com.example.whitenoise.utils;

import com.google.common.hash.*;
import com.google.common.io.*;
import com.google.common.primitives.Bytes;
import com.google.common.primitives.Longs;
import com.vdurmont.emoji.EmojiParser;

import java.io.*;
import java.util.*;
import java.nio.ByteBuffer;

public class Utils {
    public static byte[] readBlock(String path, long index, int blockSize) throws IOException {
        byte[] bytes = new byte[blockSize];

        try (RandomAccessFile file = new RandomAccessFile(path, "r")) {
            file.seek(index * blockSize);

            try {
                file.readFully(bytes);
            } catch (EOFException ignore) {
                // some part of the last block in a file will be filled with zeros
            }
        }

        return bytes;
    }

    public static long numberOfBlocks(long len, int blockSize) {
        return (len + (blockSize - 1)) / blockSize;
    }

    public static long numberOfBlocks(String path, int blockSize) {
        long len = new File(path).length();
        return numberOfBlocks(len, blockSize);
    }

    public static boolean checkFileRandomness(String path, int blockSize) throws IOException {
        HashSet<ByteBuffer> blocks = new HashSet<>();
        long blocksCount = numberOfBlocks(path, blockSize);

        for (long i = 0; i < blocksCount; i++) {
            ByteBuffer block = ByteBuffer.wrap(readBlock(path, i, blockSize));
            if (blocks.contains(block)) {
                return false;
            }
            blocks.add(block);
        }

        return true;
    }

    public static HashCode fileHashCode(String path) throws IOException {
        File file = new File(path);
        ByteSource byteSource = Files.asByteSource(file);
        return byteSource.hash(Hashing.sha256());
    }

    public static String fileStringHash(String path) throws IOException {
        return fileHashCode(path).toString();
    }

    public static long fileLongHash(String path) throws IOException {
        return fileHashCode(path).asLong();
    }

    public static HashCode bytesHashCode(byte[] bytes) {
        return Hashing.sha256().hashBytes(bytes);
    }

    public static byte[] bytesHash(byte[] bytes) {
        return bytesHashCode(bytes).asBytes();
    }

    public static long bytesLongHash(byte[] bytes) {
        return bytesHashCode(bytes).asLong();
    }

    public static byte[] mixedHash(byte[] bytes1, byte[] bytes2) {
        return bytesHash(Bytes.concat(bytesHash(bytes1), bytes2));
    }

    public static byte[] mixedHash(long value, byte[] bytes2) {
        return mixedHash(Longs.toByteArray(value), bytes2);
    }

    public static List<String> splitStringIntoEmojis(String string) {
        return EmojiParser.extractEmojis(string);
    }

    public static boolean containsOnlyEmojis(String string) {
        String emojis = String.join("", splitStringIntoEmojis(string));
        return emojis.equals(string);
    }

    public static boolean isSingleEmoji(String string) {
        return containsOnlyEmojis(string) && (splitStringIntoEmojis(string).size() == 1);
    }

    // used in tests
    public static byte[] generateSampleBytes() {
        return BaseEncoding.base16().lowerCase().decode("00010002000300040005");
    }
    // used in tests
    public static void generateSampleFile(String path) throws IOException {
        byte[] bytes = generateSampleBytes();
        File file = new File(path);
        Files.write(bytes, file);
    }
}
