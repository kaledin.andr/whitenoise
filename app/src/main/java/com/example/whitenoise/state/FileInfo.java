package com.example.whitenoise.state;

import androidx.annotation.NonNull;

import java.util.Objects;

public class FileInfo {
    @NonNull
    private String path;
    @NonNull
    private String stringHash;
    private long longHash;
    private boolean isRandom;

    public FileInfo() {
        path = "";
        stringHash = "";
    }

    public FileInfo(String path, String stringHash, long longHash, boolean isRandom) {
        this.path = path;
        this.stringHash = stringHash;
        this.longHash = longHash;
        this.isRandom = isRandom;
    }

    @NonNull
    public String getPath() {
        return path;
    }

    public void setPath(@NonNull String path) {
        this.path = path;
    }

    @NonNull
    public String getStringHash() {
        return stringHash;
    }

    public void setStringHash(@NonNull String stringHash) {
        this.stringHash = stringHash;
    }

    public long getLongHash() {
        return longHash;
    }

    public void setLongHash(long longHash) {
        this.longHash = longHash;
    }

    public boolean isRandom() {
        return isRandom;
    }

    public void setRandom(boolean random) {
        isRandom = random;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        FileInfo fileInfo = (FileInfo) o;
        return longHash == fileInfo.longHash && isRandom == fileInfo.isRandom && Objects.equals(path, fileInfo.path) && Objects.equals(stringHash, fileInfo.stringHash);
    }

    @Override
    public int hashCode() {
        return Objects.hash(path, stringHash, longHash, isRandom);
    }
}
