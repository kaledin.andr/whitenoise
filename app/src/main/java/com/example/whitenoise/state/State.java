package com.example.whitenoise.state;

import androidx.annotation.NonNull;

import com.example.whitenoise.utils.Utils;

import com.google.common.collect.Maps;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.core.type.TypeReference;
import com.google.common.hash.HashCode;

import java.io.*;
import java.util.*;


public class State {
    private final String pathToConfig;
    private final Map<String, FileInfo> emojiToFileInfoMap;
    private static final ObjectMapper mapper = new ObjectMapper();
    protected static final int BLOCK_SIZE = 32;

    public State() {
        pathToConfig = "";
        emojiToFileInfoMap = Maps.newHashMap();
    }

    public State(String path) throws IOException {
        pathToConfig = path;
        emojiToFileInfoMap = deserializeMap(path);
    }

    public String getPathToConfig() {
        return pathToConfig;
    }

    public Map<String, FileInfo> getEmojiToFileInfoMap() {
        return Collections.unmodifiableMap(emojiToFileInfoMap);
    }

    public Set<String> getRegisteredEmojis() {
        return Collections.unmodifiableSet(emojiToFileInfoMap.keySet());
    }

    public void putEmoji(String emoji, String filePath) throws IOException, IllegalArgumentException {
        if (!Utils.isSingleEmoji(emoji)) {
            throw new IllegalArgumentException("first argument should be a single emoji string");
        }

        HashCode hash = Utils.fileHashCode(filePath);
        boolean isRandom = Utils.checkFileRandomness(filePath, BLOCK_SIZE);
        FileInfo info = new FileInfo(filePath, hash.toString(), hash.asLong(), isRandom);
        emojiToFileInfoMap.put(emoji, info);
        serializeMap();
    }

    @NonNull
    public FileInfo getEmoji(String emoji) throws NoSuchElementException {
        if (!emojiToFileInfoMap.containsKey(emoji)) {
            String allEmojis = String.join("", getRegisteredEmojis());
            throw new NoSuchElementException(String.format("emoji %s not found, all emojis: %s", emoji, allEmojis));
        }
        return Objects.requireNonNull(emojiToFileInfoMap.get(emoji));
    }

    public boolean checkIntegrity(String emoji) throws NoSuchElementException, IOException {
        FileInfo info = getEmoji(emoji);
        String hash = Utils.fileStringHash(info.getPath());
        String expectedHash = info.getStringHash();
        return hash.equals(expectedHash);
    }

    public void removeEmoji(String emoji) throws IOException {
        emojiToFileInfoMap.remove(emoji);
        serializeMap();
    }

    private static Map<String, FileInfo> deserializeMap(String path) throws IOException {
        File file = new File(path);

        if (!file.exists()) {
            return Maps.newHashMap();
        } else {
            TypeReference<HashMap<String, FileInfo>> typeRef = new TypeReference<HashMap<String, FileInfo>>() {};
            return mapper.readValue(file, typeRef);
        }
    }

    private void serializeMap() throws IOException {
        File file = new File(pathToConfig);
        file.createNewFile();
        mapper.writeValue(file, emojiToFileInfoMap);
    }
}
