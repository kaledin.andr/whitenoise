package com.example.whitenoise.state;

import com.example.whitenoise.utils.Utils;

import com.google.common.base.Charsets;
import com.google.common.collect.*;
import com.google.common.io.CharSink;

import org.junit.Test;
import static org.junit.Assert.*;

import java.io.IOException;
import java.nio.file.*;
import java.util.NoSuchElementException;


public class StateTest {
    @Test
    public void empty_test() {
        State state = new State();
        assertEquals("", state.getPathToConfig());
        assertEquals(Maps.newHashMap(), state.getEmojiToFileInfoMap());
    }

    @Test
    public void simple_test() throws IOException, NoSuchElementException {
        String path = "tmp.json";

        State state = new State(path);
        assertEquals(path, state.getPathToConfig());

        String emoji1 = "❤";
        String emoji2 = "❌";
        String path1 = "1";
        String path2 = "2";

        Utils.generateSampleFile(path1);
        Utils.generateSampleFile(path2);

        state.putEmoji(emoji1, path1);
        state.putEmoji(emoji2, path2);

        try {
            state.putEmoji(emoji1 + emoji2, path1);
        } catch (IllegalArgumentException exception) {
            assertEquals(exception.getMessage(), "first argument should be a single emoji string");
        }

        assertEquals(Sets.newHashSet(emoji1, emoji2), state.getRegisteredEmojis());

        for (String emoji : new String[]{emoji1, emoji2}) {
            FileInfo info = state.getEmoji(emoji);
            assertEquals(info.getStringHash(), Utils.fileStringHash(path1));
            assertEquals(info.getLongHash(), Utils.fileLongHash(path1));
            assertTrue(info.isRandom());
            assertTrue(state.checkIntegrity(emoji));
        }
        assertEquals(state.getEmoji(emoji1).getPath(), path1);
        assertEquals(state.getEmoji(emoji2).getPath(), path2);

        State state2 = new State(path);
        assertEquals(state.getEmojiToFileInfoMap(), state2.getEmojiToFileInfoMap());
        assertEquals(state.getPathToConfig(), state2.getPathToConfig());

        state2.removeEmoji("❌");
        assertEquals(Sets.newHashSet("❤"), state2.getRegisteredEmojis());

        String hello = "Hello, world";
        CharSink sink = com.google.common.io.Files.asCharSink(new java.io.File(path1), Charsets.UTF_8);
        sink.write(hello);

        assertFalse(state.checkIntegrity(emoji1));

        Files.delete(Paths.get(path));
        Files.delete(Paths.get(path1));
        Files.delete(Paths.get(path2));
    }
}