package com.example.whitenoise.model;

import com.example.whitenoise.utils.Utils;
import com.google.common.collect.Lists;

import org.junit.Test;
import static org.junit.Assert.*;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.List;

public class ModelTest {
    @Test
    public void model_test() throws IOException {
        String path = "tmp.json";
        Model model = new Model(path);

        String key = "❤❌⬛";
        List<String> emojis = Utils.splitStringIntoEmojis(key);
        List<String> paths = Lists.newArrayList("1", "2", "3");

        for (int i = 0; i < emojis.size(); i++) {
            Utils.generateSampleFile(paths.get(i));
            model.putEmoji(emojis.get(i), paths.get(i));
        }

        byte[] message = Utils.generateSampleBytes();
        byte[] encoded = model.encodeDecode(message, key);
        byte[] decoded = model.encodeDecode(encoded, key);
        assertArrayEquals(message, decoded);

        for (String filePath : paths) {
            Files.delete(Paths.get(filePath));
        }
        Files.delete(Paths.get(path));
    }
}
