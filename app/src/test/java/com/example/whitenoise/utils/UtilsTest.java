package com.example.whitenoise.utils;

import static org.junit.Assert.*;

import androidx.core.util.Pair;

import com.google.common.collect.Lists;

import org.junit.Test;

import java.io.*;
import java.nio.file.Paths;
import java.util.List;

public class UtilsTest {
    @Test
    public void simple_test() throws IOException {
        String path = "tmp";

        Utils.generateSampleFile(path);
        byte[] bytes = Utils.generateSampleBytes();

        assertArrayEquals(bytes, Utils.readBlock(path, 0, 10));
        assertEquals(2, Utils.numberOfBlocks(path, 8));
        assertTrue(Utils.checkFileRandomness(path, 2));
        assertFalse(Utils.checkFileRandomness(path, 1));

        // echo -n "00010002000300040005" | xxd -r -p | sha256sum -b | awk '{print $1}'
        String hash = "a952577e7fb3a2f24c293362b4979876d15343cb9bedf0e999fe96b534faf7ef";
        assertEquals(hash, Utils.fileStringHash(path));

        java.nio.file.Files.delete(Paths.get(path));
    }

    @Test
    public void emoji_split_test() {
        List<Pair<List<String>, String>> tests = Lists.newArrayList(
            new Pair<>(Lists.newArrayList("🙁", "🙂", "🙁"), "🙁🙂🙁"),
            new Pair<>(Lists.newArrayList("😂", "😍", "🎉", "👍"), "😂😍🎉👍"),
            new Pair<>(Lists.newArrayList("👨‍👨‍👧‍👧", "👦🏾"), "👨‍👨‍👧‍👧👦🏾")
        );

        for (Pair<List<String>, String> test : tests) {
            assertEquals(test.first, Utils.splitStringIntoEmojis(test.second));
        }

        assertTrue(Utils.containsOnlyEmojis("👨‍👨‍👧‍👧👦🏾"));
        assertFalse(Utils.containsOnlyEmojis("b👨‍👨‍👧‍👧a👦🏾c"));
        assertFalse(Utils.containsOnlyEmojis("abc"));

        assertTrue(Utils.isSingleEmoji("👨‍👨‍👧‍👧")); // family emoji
        assertTrue(Utils.isSingleEmoji("👦🏾")); // another compound emoji
        assertTrue(Utils.isSingleEmoji("🙁")); // normal emoji
        assertFalse(Utils.isSingleEmoji("🙁🙂")); // multiple emojis
        assertFalse(Utils.isSingleEmoji("a")); // not emoji
        assertFalse(Utils.isSingleEmoji("a🙂")); // contains non-emoji character
    }
}
