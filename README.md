# WhiteNoise

## Motivation

According to the paper "Communication Theory of Secrecy Systems", published in 1949 by Claude Shannon, theoretically unbreakable cipher must have the number of different keys at least as great as the number of different plaintext.

One example of such cipher is Vernam's cipher (one-time pad), which is one of the simplest cryptographic systems that provides perfect secrecy.

Vernam's cipher encodes a message by combining it's bits with secret key's bits using xor (exclusive or) operation.
Secret key must be randomly generated, and have the same size as the message.
Also one secret key must only be used once, and then a new key must be generated.

Algorithm for decoding a message is exactly the same as encoding it, since $(m \oplus s) \oplus s = m \oplus (s \oplus s) = m \oplus 0 = m$, where $m$ is one bit of a message, $s$ is one bit of a secret key, $m \oplus s$ is one bit of encoded message, $\oplus$ is xor operation.

While this algorithm is very simple and provides perfect secrecy, it has a few issues:

1. It is difficult to generate high-quality random numbers.
2. Key distribution: it's necessary to distribute long secret keys, which is inconvenient and poses a significant security risk.

We have built an android application, which implements a version of Vernam's cipher, and tries to mitigate some of these issues.

## Approach
1. Users have to somehow distribute between themselves several large files with sufficiently-random bytes. One way to do that, is to write files onto an SD card, exchange SD card $n$ times, and then destroy it. We consider file to be sufficiently random if it does not have two blocks of 32 bytes with the same value.
2. Each file $f_i$ is associated with an emoji $e_i$, to identify this file.
3. To encode a message, users input a key - which is a sequence of emojis $k = e_1, \ldots, e_n$ and a message $m$ which they want to encode.
   1. Our application first combines different parts of files $f_i$ that correspond to emojis $e_i$ into one sequence of bytes $s$ that is the same length as $m$
   2. Then application calculates $r  = m \oplus s$
   3. Then application shows result $r$ to the user.
4. Users send $r$ to other users, for example via open channels.
5. Then they decode $r$ (using the same algorithm as for encoding).

### Generating sequence of bytes
Suppose we have files $f_1, \ldots, f_n$, and we want to generate some random sequence of bytes $s$ with length $\geq l$ using these files as sources of random data (white noise). Here is how our application does that:

1. Each file is split into blocks of 32 bytes, $f_i[j]$ is the block $j$ from the file $f_i$.
2. Files are considered to be cyclic, that is, if file $f_i$ has $m$ blocks in it, then $f_i[j] = f_i[j+m]=f_i[j-m]$ for all $i, j$.
3. First, we initialize indexes of blocks in files as $p_i^0$ = SHA($f_i$), where SHA is SHA-256 hash function.
4. Then, we generate $k$ blocks $s_1, \ldots, s_k$, where $k$ = ceil($l / 32$).
5. Each block $s_t$ is generated as $s_t$=SHA(SHA($f_2[p_2^t]$), SHA(SHA($f_1[p_1^t])$, $f_0^t[p_0^t]$))) (example for $n=3$).
6. Then we update indexes: $p_i^{t+1}$=SHA(SHA($p_i^t$), SHA(SHA($g$), $f_i[p_i^t]$)), where
   * $g$=SHA(SHA($p_2^t$), SHA(SHA($f_2[p_2^t]$), SHA(SHA($p_1^t$), SHA(SHA($f_1[p_1^t]$), SHA(SHA($p_0^t$), SHA(SHA($f_0[p_0^t]$), $s_t$)))))) (example for $n=3$).
7. Then we concatenate those blocks: $s = s_1, \ldots, s_k$ and return result.


## How to build
We used Android Studio 2022.1.1 and Gradle to build an android application. Application is written in Java.

## How to use

Video demo of the application (in russian): https://youtu.be/27lkYpxtZ8Q

The application has two screens that you can use to:
1. Create (or remove) an association between emoji $e_i$ and file with white noise $f_i$.
2. Encrypt or decrypt a message using a sequence of emojis.

   <img src="screenshot1.jpg" height="400">
   <img src="screenshot2.jpg" height="400">

## Some observations
1. Suppose there are $n$ files in the group of $k$ people.
2. Each person has a subset of these files, and there are $2^n$ different subsets.
3. There are $n!$ different keys to encrypt messages.
4. If attacker does not know at least one of the files that were used to encrypt a message, then it would be quite difficult to decrypt a message (we have not analyzed this in detail, however).
5. Files can be shared using an epidemic protocol, so distribution can be relatively efficient.
6. Files can be relatively large (e.g. 32 GB), and still be stored on a small SD card.
7. It would be very difficult to steal files from an offline device.

## Additional analysis
Some additional analysis (in russian) on how this approach can scale and function, in spite of presence of malicious members, can be found at https://github.com/decentralized-hse/white-noise-analysis
